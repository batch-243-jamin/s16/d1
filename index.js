// console.log("Hello World");

// [Section] Arithmetic Operators

let x = 1397;
console.log("The value of x is " + x);
let y = 7831;
console.log("The value of y is " + y);

// Addition Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Difference Operator
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of division operator: " + quotient);

// Modulo
let remainder = y % x;
console.log("Result of modulo: " + remainder);

let secondRemainder = x % y;
console.log("Result of modulo: " + secondRemainder);

// [Section] Assignment Operators
	// Basic assignment Operator(=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the value.

	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// assignmentNumber += 2;
	// console.log(assignmentNumber);

	// Subraction Assignment Operator (-=)
	assignmentNumber -= 2;
	console.log( "Result of addition subtraction operator: " + assignmentNumber);

	// Multiplication Assignment Operator (*=)

	assignmentNumber *=

	assignmentNumber *= 4;
	console.log("Result of multiplication assignment operator:" + assignmentNumber);

	// Division Assignment Operator (/=)
	assignmentNumber /= 8;
	console.log("Result of division assignment operator: " + assignmentNumber);

	// Multiple Operators and Parenthesis
	/*
		1. 3*4 = 12
		2. 12 / 5 = 2.4
		3. 1+2 = 3
		4. 3-2.4 = 0.6
	*/
	let mdas = 1 + 2 - 3 * 4 /5;
	console.log("Result of mdas rule: " + mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas rule: " + pemdas);

// [Section] Incrementation and Decrement	
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to.

	let z = 1;

	// Pre-increment
	let increment = ++z;
	console.log("Result of z in pre-increment: " + z);
	console.log("Result of increment in pre-increment: " + increment);

	// Post-increment
	increment = z++;
	console.log("Result of z in post-increment: " + z);
	console.log("Result of increment in post-increment: " + increment);
	increment = z++;
	console.log("Result of increment in post-increment: " + increment);

	let p = 0;
	// pre-decrement

	let decrement = --p;
	console.log("Result of z in pre-decrement: " + p);
	console.log("Result of decrement in pre-decrement: " + decrement);

	//post-decrement
	decrement = p--;
	console.log("Result of p in pre-decrement: " + p);
	console.log("Result of decrement in post-decrement: " + decrement);
	decrement = p--;
	console.log("Result of decrement in post-decrement: " + decrement);

// [Section]Type Coercion
	/*
		-Type coercion is the automatic or implicit conversion of values from one data type to another.
		-This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
		-Values are automatically converted from one data type to another in order to resolve operations.


	*/

	let numA = '10';
	let numB = 12;
	/*
		-adding or concatinating a string and a number will result into string
		-This can be proven in the console by looking at the color of the text display.
	*/

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	// Non-coercion
	let numC = 16;
	let numD = 14;
	let nonCoersion = numC + numD;
	console.log(nonCoersion);
	console.log(typeof nonCoersion);

	let numE = false + 1;
	console.log(numE);
	/*
		Type Coercion: Boolean value and number
		The result will be a number
		The boolean value will be converted. true = 1, false = 0
	*/
// [Section] Comparison Operators
	let juan = 'juan';

	// Equality operator (==)
	/*
		-checks whether the operands are equal/ have the same value
	*/
	let isEqual = 1 == 1;
	console.log( typeof isEqual);

	console.log(1 == 2);
	console.log(1 == "1");

	// Strict equality operator (===)
	console.log(1 === '1');
	console.log('juan' == 'juan');

	// Inequality operator (!=)
	/*
		-checks whether the operands are not equal/ have differenct content
		-attempts to convert and compare operands of different data types
	*/

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');

	// Strict Inequality operator
	console.log(1 !== 1);

	// [Section] Relational Operators
	// Some comparison operators check whether one value is greater or less than to the other value.

	let a = 50;
	let b = 65;

	// GT or Greater Than operator (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	// LT or Less Than operator (<)
	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE or Greater Than or Equal operator (>=)
	let isGTorEqual = a>= b;
	console.log(isGTorEqual);

	// LTE or Less Than or Equal operator (<=)
	let isLTOrEqual = a<=b;
	console.log(isLTOrEqual);

	let numStr = "30";

	console.log( a > numStr);

	console.log(b <= numStr);

	// [Section] Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&& - double ampersand)
	// returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of Logical and operator: " + allRequirementsMet);

	// Logical Or Operator (|| - double pipe)
	// Returns true if one of the operand is are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of Logical or Operator: " + someRequirementsMet);

	// Logical Not Operator (! - Exclamation Point)
	let someRequirementsNotMet = !isRegistered;
	console.log(someRequirementsNotMet);
































































